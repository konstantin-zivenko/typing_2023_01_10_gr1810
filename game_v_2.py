import random
from typing import List, Sequence, Tuple, Any, TypeVar

SUITS = "♠ ♡ ♢ ♣".split()  # создаем список мастей
RANKS = "2 3 4 5 6 7 8 9 10 J Q K A".split()  # создаем список рангов

Card = Tuple[str, str]
Deck = List[Card]

Choosable = TypeVar("Choosable")



def create_deck(shuffle: bool = False) -> Deck:
    """Создаем новую колоду. При необходимости - тасуем (shuffle=True)"""
    deck = [(s, r) for r in RANKS for s in SUITS]
    if shuffle:
        random.shuffle(deck)
    return deck


def deal_hands(deck: Deck) -> Tuple[Deck, Deck, Deck, Deck]:
    """сдает колоду четырем игрокам"""
    return (deck[0::4], deck[1::4], deck[2::4], deck[3::4])


def choose(items: Sequence[Choosable]) -> Choosable:
    """выбирает и возвращает случайный элемент из набора"""
    return random.choice(items)


def player_order(names: Sequence[str], start=None):
    """возвращает последовательность ходов игроков"""
    if start is None:
        start = choose(names)
    start_idx = names.index(start)
    return names[start_idx:] + names[:start_idx]


def play() -> None:
    """реализует цикл игры: создает колоду, тасует, раздает четырм игрокам"""
    deck = create_deck(shuffle=True)
    names = "P1 P2 P3 P4".split()
    hands = {n: h for n, h in zip(names, deal_hands(deck))}
    start_player = choose(names)
    turn_order = player_order(names, start=start_player)

    # игроки по очереди ходят случайными картами пока они не закончатся
    while hands[start_player]:
        for name in turn_order:
            card = choose(hands[name])
            hands[name].remove(card)
            print(
                f"{name}: {card[0] + card[1]:<3}  ", end=""
            )  # для понимакния строки - читаем документацию
            # https://docs.python.org/3/library/string.html#formatstrings
        print()


if __name__ == "__main__":
    play()
